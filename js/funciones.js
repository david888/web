
function validarFormulario(){
    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var fechaNacimiento = document.getElementById("fecha_nacimiento").value;
    
    var sexo = undefined;
    var radios = document.getElementsByName("sexo");

    for (let index = 0; index < radios.length; index++) {
        if(radios[index].checked){
            sexo = radios[index].value;
            break;
        }
    }
    var valoracion = document.getElementById("valoracion").value;
    var email = document.getElementById("email").value;
    
   
    if(nombre == "" || apellido == "" || fechaNacimiento == "" ||sexo == undefined || valoracion == "" || email == ""){
      alert("Algunos campos son obligatorios");  
      return false;
    }

    var datos = "Nombre:" + nombre + "\nApellido:" + apellido + "\nFecha de nacimiento:" + fechaNacimiento + "\nValoracion:" + valoracion + "\nMail:" + email + "\nSexo:" + sexo;
    alert(datos);
    return true;
}

function confirmacion(){
    var confirmar = confirm("Desea volver a la pagina anterior");

    if (confirmar) {
      location.href = "index.html" 
    } 
}
